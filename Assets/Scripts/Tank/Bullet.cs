﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public System.Action<Bullet,Collision2D> collisionCallback;

    Rigidbody2D rigid;
    public float speed;
    public GameObject hitEffect;

    void Awake()
    {
        if (rigid == null)
            rigid = GetComponent<Rigidbody2D>();
    }

    private void OnValidate()
    {
        if (rigid == null)
            rigid = GetComponent<Rigidbody2D>();
    }

    public void Shoot(Vector2 direction)
    {
        rigid.velocity = direction.normalized * speed;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (hitEffect != null)
        {
            GameObject go = GameObject.Instantiate(hitEffect, transform.position, Quaternion.identity) as GameObject;
            AudioSource audio = go.GetComponent<AudioSource>();
            if (audio != null) audio.Play();
        }
        if (collisionCallback != null) collisionCallback(this,coll);
    }
}
