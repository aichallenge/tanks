﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public enum RoundEndedMotive
    {
        TankWin,
        TimesUp
    }

    public class TankIntel
    {
        public TankIntel(Tank tank)
        {
            this.tank = tank;
        }

        private Tank tank;

        public string JsonInfo()
        {
            string json = "";

            json = " { \"name\":\"" + tank.TankScheme.tankName + "\"" +
                ", \"position\":" + JsonUtility.ToJson(tank.transform.position) + ", \"aim\": " + tank.barrel.BarrelAngleDegree + ", \"bullets\":[";

            foreach(Bullet b in tank.ActiveBullets())
            {
                json += JsonUtility.ToJson(b.transform.position) + ",";
            }

            if (tank.ActiveBullets().Length > 0)
            {
                json = json.Remove(json.Length - 1, 1); // remove last comma
            }

            return json + "]}";
        }
    }

    public class TanksIntel
    {
        public TanksIntel(Tank [] _tanks)
        {
            tanks = new TankIntel[_tanks.Length];
            for (int i = 0; i < _tanks.Length; ++i)
            {
                tanks[i] = new TankIntel(_tanks[i]);
            }
        }
        TankIntel[] tanks;

        public string JsonInfo()
        {
            string json = "{ \"tanks\":[";
           
            foreach(TankIntel intel in tanks)
            {
                json += intel.JsonInfo() + ",";
            }

            if (tanks.Length > 0)
            {
                json = json.Remove(json.Length - 1, 1);
            }

            return json + "]}";
        }
    }

    public event System.Action<GameController, RoundEndedMotive> RoundEnded;
    public event System.Action<GameController> GameOver;

    private static GameController instance;
    public static GameController Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject go = new GameObject("GameController");
                DontDestroyOnLoad(go);
                instance = go.AddComponent<GameController>();
            }

            return instance;
        }
    }

    private Tank[] tanks;

    public Tank[] Tanks
    {
        get
        {
            return tanks;
        }

        set
        {
            tanks = value;
            intel = new TanksIntel(value);
        }
    }

    public TankScheme LastVictory
    {
        get
        {
            if (victorySequence.Count == 0) return null;
            return victorySequence[victorySequence.Count - 1];
        }
    }

    public List<TankScheme> victorySequence = new List<TankScheme>();
    public GameInfo gameInfo = new GameInfo();
    public GameScoreControl scoreControl = new GameScoreControl("Testing");
    private string scoreFileName = "Scores/_Testing.json";
    private TanksIntel intel;

    public void Restart(string level)
    {
        victorySequence.Clear();
        scoreControl = new GameScoreControl(level);
        scoreFileName = "Scores" + "/" + level + "_" + System.DateTime.Now.ToString("dd_MM_HH_mm_ss") + ".score.json";
        Round = 0; // Level  loader will increase before it starts the levle
    }

    public string IntelJson()
    {
        return intel.JsonInfo();
    }

    public int Round;

    [System.Serializable]
    public class GameInfo
    {
        [SerializeField]
        private int MaxKillPerRound = 5; // as a UI restriction

        public int KillPerRoundToVictory = 3;
        public int Rounds = 3;
        public int ScoreByKill = 50;
    }

    private float updateIntelTime = 0.150f;
    private float updateIntelTimer = 0;
    private void Update()
    {
        Tank[] tanks = this.Tanks;
        if (tanks == null) return;

        //update tanks report
        //counting a time to avoid network busy
        updateIntelTimer += Time.deltaTime;
        if (updateIntelTimer > updateIntelTime)
        {
            ServerController.Instance.BroadCastMessage(intel.JsonInfo());
            updateIntelTimer = 0;
        }

        if (GameTimer.Instance.IsOver)
        {
            Debug.Log("Times up");
            TurnEnded(RoundEndedMotive.TimesUp);
            if (Round == gameInfo.Rounds)
            {
                _GameOver();
            }
            enabled = false;
            //SceneManager.LoadScene("TankBattle"); // as testing for now
            return;
        }

        foreach(Tank tank in tanks)
        {
            if (!tank.gameObject.activeSelf) continue;
            if (tank.Kills >= gameInfo.KillPerRoundToVictory)
            {
                Debug.Log("Tank won");
                TurnEnded(RoundEndedMotive.TankWin);
                if (Round == gameInfo.Rounds)
                {
                    _GameOver();
                }

                enabled = false;
                //SceneManager.LoadScene("TankBattle"); // as testing for now
                break;
            }
        }
    }
    
    private void TurnEnded(RoundEndedMotive motive)
    {
        GameTimer.Instance.Stop();
        Tank[] tanks = this.Tanks;
        if (tanks == null)
        {
            Debug.LogWarning("Tanks are null");
            return;
        }
        System.Array.Sort<Tank>(tanks, delegate (Tank x, Tank y)
        {
            return y.Score.CompareTo(x.Score);
        });

        if (motive == RoundEndedMotive.TankWin)
            victorySequence.Add(tanks[0].TankScheme);
        else
            victorySequence.Add(null);

        foreach (Tank tank in tanks)
        {
            tank.CommunicationSuspended = true;
        }

        ServerController.Instance.BroadCastMessage("Round Finished. Winner [" + tanks[0].TankScheme.tankName + "]");
        scoreControl.FinishRound(tanks);
        SaveRound();
        if (RoundEnded!=null) RoundEnded(this, motive);
    }

    private void SaveRound()
    {
        string json = JsonUtility.ToJson(scoreControl);
        System.IO.File.WriteAllText(scoreFileName, json);
    }

    private void _GameOver()
    {
        Debug.Log("GameOver");
        ServerController.Instance.BroadCastMessage("GameOver. Winner [" + tanks[0].TankScheme.tankName + "]");

        if (GameOver != null)
        {
            GameOver(this);
        }
    }
}
