﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GameScoreControl {

    [System.Serializable]
    public class TankScore
    {
        public string tankName = "";
        public float score = 0;
        public int deaths = 0;
        public int kills = 0;

        public TankScore (TankScore _score)
        {
            this.tankName = _score.tankName;
            this.score = _score.score;
            this.deaths = _score.deaths;
            this.kills = _score.kills;
        }

        public TankScore (Tank tank)
        {
            this.tankName = tank.TankScheme.tankName;
            this.score = tank.Score;
            this.deaths = tank.Deaths;
            this.kills = tank.Kills;
        }
    }

    [System.Serializable]
    public class RoundScore
    {
        public int roundNumber;
        public List<TankScore> scores = new List<TankScore>();

        public RoundScore(int r)
        {
            roundNumber = r;
        }
    }

    [System.Serializable]
    public class GameScore
    {
        public List<RoundScore> rounds = new List<RoundScore>();
    }

    public GameScore game;

    public string levelName;
    
    public GameScoreControl(string levelName)
    {
        this.levelName = levelName;
        game = new GameScore();
    }

    public void FinishRound(Tank [] tanks)
    {
        System.Array.Sort(tanks, delegate (Tank t, Tank t2)
        {
            return t2.Score.CompareTo(t.Score);
        });

        RoundScore round = new RoundScore(game.rounds.Count);
        foreach (Tank tank in tanks)
        {
            TankScore score = new TankScore(tank);
            round.scores.Add(score);
        }
        game.rounds.Add(round);
    }

    public TankScore TotalScoreFor(Tank tank)
    {
        TankScore[] scores = TotalScores();
        foreach(TankScore s in scores)
        {
            if (s.tankName == tank.TankScheme.tankName) return s;
        }

        return null;
    }

    public TankScore[] TotalScores()
    {
        List<TankScore> scores = new List<TankScore>();
        foreach (RoundScore round in game.rounds)
        {
            foreach (TankScore score in round.scores)
            {
                TankScore _score = scores.Find(s=> { return s.tankName == score.tankName; });
                if (_score == null)
                {
                    _score = new TankScore(score);
                    scores.Add(_score);
                    continue;
                }

                _score.score += score.score;
                _score.kills += score.kills;
                _score.deaths += score.deaths;
            }
        }

        return scores.ToArray();
    }

    public TankScore BestScoreOfAll()
    {
        float bestScore = 0;
        TankScore _score = null;
        foreach (RoundScore round in game.rounds)
        {
            foreach(TankScore score in round.scores)
            {
                if (bestScore <= score.score)
                {
                    _score = score;
                    bestScore = score.score;
                }
            }
        }

        return _score;
    }
}
