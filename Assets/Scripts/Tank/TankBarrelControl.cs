﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TankBarrelControl : MonoBehaviour {

    public System.Action<TankBarrelControl> AimCompleted; // aim animation has been completed
    public System.Action<TankBarrelControl, float> FireCompleted; // fire was possible, the fire happen at this direction 
    public System.Action<TankBarrelControl> CoolDownCompleted; // cool down complete. it is able to fire again
    public System.Action<TankBarrelControl, Collision2D> FireHit; // the bullet hit something. collision info


    public Sprite bulletSprite;

    [SerializeField]
    private Transform barrel;

    [SerializeField]
    private GameObjectPool pool;

    [SerializeField]
    private float coolDownTime = 1;

    [SerializeField]
    ParticleSystem fireEffect;

    private List<Bullet> activeBulllets = new List<Bullet>();

    private bool canFire = true;

    
    public float CoolDownTimer
    {
        get
        {
            return coolDownTimer;
        }
    }

    public bool CanFire
    {
        get
        {
            return canFire;
        }
    }

    public float BarrelAngleDegree
    {
        get
        {
            return barrel.transform.eulerAngles.z;
        }
    }

    //For animation

    [SerializeField]
    private float timeToAdjust = 0.5f;

    private bool adjustBarrelDirection = false;
    private Quaternion currentOrientation;
    private Quaternion targetOrientationDegree;
    private float adjustBarrelAnimationTimer = 0;

    private float coolDownTimer = 0;
   

    public void Aim(float angle)
    {
        currentOrientation = barrel.rotation;
        targetOrientationDegree = Quaternion.Euler(new Vector3(0, 0, angle * Mathf.Rad2Deg));
        adjustBarrelDirection = true;
        adjustBarrelAnimationTimer = 0;
    }

    public Bullet[] GetActiveBullets()
    {
        return activeBulllets.ToArray();
    }

    public bool Fire()
    {
        if (!canFire) return false;

        Bullet bullet = pool.GetObject().GetComponent<Bullet>();
        bullet.GetComponentInChildren<SpriteRenderer>().sprite = bulletSprite;
        Physics2D.IgnoreCollision(bullet.GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());
        bullet.transform.position = barrel.transform.position;
        bullet.transform.rotation = barrel.rotation;
        bullet.Shoot(Util.VectorFromDegreeAngle(barrel.rotation.eulerAngles.z));
        bullet.collisionCallback = BulletCallback;

        activeBulllets.Add(bullet);

        canFire = false;
        coolDownTimer = 0;

        if (fireEffect != null)
        {
            fireEffect.Play();
            AudioSource sfx = fireEffect.GetComponent<AudioSource>();
            if (sfx != null) sfx.Play();
        }

        if (FireCompleted != null)
        {
            FireCompleted(this, barrel.rotation.eulerAngles.z);
        }


        return true;
        
    }

    public void BulletCallback(Bullet bullet, Collision2D collision)
    {
        Debug.Log("Bullet hit something");
        pool.PutOnPull(bullet.gameObject);
        activeBulllets.Remove(bullet);

        if (FireHit != null)
        {
            FireHit(this, collision);
        }
    }

    void Update()
    {
        coolDownTimer += Time.deltaTime;
        if (!canFire && coolDownTimer >= coolDownTime)
        {
            canFire = true;

            if (CoolDownCompleted != null)
            {
                CoolDownCompleted(this);
            }
        }

        if (adjustBarrelDirection)
        {
            adjustBarrelAnimationTimer = Mathf.Min(adjustBarrelAnimationTimer + Time.deltaTime, timeToAdjust);
            barrel.rotation = Quaternion.Lerp(currentOrientation, targetOrientationDegree, adjustBarrelAnimationTimer / timeToAdjust);

            if (adjustBarrelDirection && adjustBarrelAnimationTimer >= timeToAdjust)
            {
                adjustBarrelDirection = false;
                if (AimCompleted!= null)
                {
                    AimCompleted(this);
                }                
            }
        }
    }

}
