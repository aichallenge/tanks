﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class RoundEndedPanel : MonoBehaviour {

    public Text currentRound;
    public RectTransform bulletParent;
    public List<Image> victoryBullets = new List<Image>();
    public Sprite timeBullet;
    public Image emptyBullet;

    public void Show(float delay)
    {
        currentRound.text = GameController.Instance.Round + "/" + GameController.Instance.gameInfo.Rounds;
        int i = 0;
        foreach (TankScheme victorySequence in GameController.Instance.victorySequence)
        {
            if (victorySequence == null)
            {
                victoryBullets[i++].sprite = timeBullet;
            } else
            {
                victoryBullets[i++].sprite = victorySequence.bulletSpriteOutline;
            }
            
        }

        StartCoroutine(HideAfterDelay(delay));
    }

    IEnumerator HideAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        gameObject.SetActive(false);
    }

	void Awake () {
        victoryBullets.Add(emptyBullet);
        for (int i = 1; i < GameController.Instance.gameInfo.Rounds; ++i)
        {
            GameObject bullet = GameObject.Instantiate(emptyBullet.gameObject) as GameObject;
            bullet.transform.SetParent(bulletParent);
            victoryBullets.Add(bullet.GetComponent<Image>());
        } 
        
    }
}