﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using UnityEngine;
using System.Collections.Generic;

public class Client
{
    public Client() { }

    public System.Action<string> messageReceived;
    public System.Action connected;    

    private Thread readingThread;
    private Thread writingThread;

    private bool isWriting;
    private bool isReading;
    private List<string> messages = new List<string>();


    public void Connect(string ip, int port)
    {
        new Thread(() =>
        {
            TcpClient client = new TcpClient(ip, port);

            if (connected != null)
            {
                connected();
            }

            Debug.Log("<color=yellow>Client Connected</color>");

            isWriting = true;
            isReading = true;

            readingThread = new Thread(() => { Read(client); });
            writingThread = new Thread(() => { Write(client); });

            readingThread.Start();
            writingThread.Start();

        }).Start();
    }

    public void SendMessage(string message)
    {
        Thread t = new Thread(() => { _SendMessage(message); });
        t.Start();
    }

    public void Close()
    {
        Thread t = new Thread(() => { _Close(); });
        t.Start();
    }

    void _Close()
    {
        isReading = false;
        isWriting = false;

        Thread.Sleep(10);

        readingThread.Abort();
        writingThread.Abort();
    }

    void _SendMessage(string message)
    {
        lock (messages)
        {
            messages.Add(message);
        }
    }

    void Read(TcpClient client)
    {
        NetworkStream nwStream = client.GetStream();
        byte[] bytesToRead = new byte[client.ReceiveBufferSize];

        while (isReading)
        {
            int bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
            string message = Encoding.UTF8.GetString(bytesToRead, 0, bytesRead);

            if (messageReceived != null)
            {
                messageReceived(message);
            }

            Thread.Sleep(1);
        }
    }

    void Write(TcpClient client)
    {
        NetworkStream nwStream = client.GetStream();
        while (isWriting)
        {
            lock (messages)
            {
                if (messages.Count > 0)
                {
                    foreach (string message in messages)
                    {
                        byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(message);
                        nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                    }
                    messages.Clear();
                }
            }

            Thread.Sleep(5);
        }
    }










    /*
    static int id = 0;
    const int PORT_NO = 5000;
    const string SERVER_IP = "127.0.0.1";
    public void StartClient()
    {
        //---data to send to the server---
        string textToSend = DateTime.Now.ToString();

        Debug.Log("Trying to connect");
        //---create a TCPClient object at the IP and port no.---
        TcpClient client = new TcpClient(SERVER_IP, PORT_NO);
        NetworkStream nwStream = client.GetStream();

        int me = id++;
        int index = 0;

        Debug.Log("ME: " + me);
        for (int i = 0; i < 100; ++i)
        {
            Debug.Log(i);
            byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(me + " => " + textToSend + index.ToString());
            index++;
            //---send the text---
            //Debug.Log(me + " => Sending : " + textToSend);
            nwStream.Write(bytesToSend, 0, bytesToSend.Length);

            //---read back the text---
            byte[] bytesToRead = new byte[client.ReceiveBufferSize];
            int bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
            //Debug.Log(me + " <= Received : " + Encoding.ASCII.GetString(bytesToRead, 0, bytesRead));
            nwStream.Flush();
        }
        
        client.Close();
    }*/
}

/*
public class AsynchronousClient
{
    // The port number for the remote device.
    private const int port = 11000;

    // ManualResetEvent instances signal completion.
    private ManualResetEvent connectDone =
        new ManualResetEvent(false);
    private ManualResetEvent sendDone =
        new ManualResetEvent(false);
    private ManualResetEvent receiveDone =
        new ManualResetEvent(false);

    // The response from the remote device.
    private String response = String.Empty;

    public static void StartClient()
    {
        AsynchronousClient l = new AsynchronousClient();
        Thread newThread = new Thread(l._StartClient);
        newThread.Start();
    }

    static bool active = true;
    static int index;
    private void _StartClient()
    {
        // Connect to a remote device.
        try
        {
            // Establish the remote endpoint for the socket.
            // The name of the 
            // remote device is "host.contoso.com".
            IPHostEntry ipHostInfo = Dns.GetHostEntry("localhost");
            IPAddress ipAddress = ipHostInfo.AddressList[1];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

            // Create a TCP/IP socket.
            Socket client = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            // Connect to the remote endpoint.
            client.BeginConnect(remoteEP,
                new AsyncCallback(ConnectCallback), client);
            connectDone.WaitOne();

            int i = 0;
            int me = index++;
            //while (active)
            for (i = 0; i < 10; ++i)
            {
                // Send test data to the remote device.
                Send(client, me + "=> This is a test " + i++ + "<EOF>");
                sendDone.WaitOne();

                // Receive the response from the remote device.
                Receive(client);
                receiveDone.WaitOne();
            }

            // Write the response to the console.
            UnityEngine.Debug.Log("Response received : " + response);

            // Release the socket.
            client.Shutdown(SocketShutdown.Both);
            client.Close();

        }
        catch (Exception e)
        {
            UnityEngine.Debug.Log(e.ToString());
        }
    }

    private void ConnectCallback(IAsyncResult ar)
    {
        try
        {
            // Retrieve the socket from the state object.
            Socket client = (Socket)ar.AsyncState;

            // Complete the connection.
            client.EndConnect(ar);

            UnityEngine.Debug.Log("Socket connected to " + client.RemoteEndPoint.ToString());

            // Signal that the connection has been made.
            connectDone.Set();
        }
        catch (Exception e)
        {
            UnityEngine.Debug.Log(e.ToString());
        }
    }

    private void Receive(Socket client)
    {
        try
        {
            // Create the state object.
            StateObject state = new StateObject();
            state.workSocket = client;

            // Begin receiving the data from the remote device.
            client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReceiveCallback), state);
        }
        catch (Exception e)
        {
            UnityEngine.Debug.Log(e.ToString());
        }
    }

    private void ReceiveCallback(IAsyncResult ar)
    {
        UnityEngine.Debug.Log("Trying to receive data");
        try
        {
            UnityEngine.Debug.Log("1");
            // Retrieve the state object and the client socket 
            // from the asynchronous state object.
            StateObject state = (StateObject)ar.AsyncState;
            Socket client = state.workSocket;

            // Read data from the remote device.
            int bytesRead = client.EndReceive(ar);
            UnityEngine.Debug.Log("2 - " + bytesRead);
            if (bytesRead > 0)
            {
                // There might be more data, so store the data received so far.
                state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                // Get the rest of the data.
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
                UnityEngine.Debug.Log("3");
            }
            else
            {
                UnityEngine.Debug.Log("4 - " + state.sb.Length);
                // All the data has arrived; put it in response.
                if (state.sb.Length > 1)
                {
                    response = state.sb.ToString();
                    UnityEngine.Debug.Log("Received " + response);
                }
                // Signal that all bytes have been received.
                receiveDone.Set();
                UnityEngine.Debug.Log("5");
            }
        }
        catch (Exception e)
        {
            UnityEngine.Debug.Log(e.ToString());
        }
        UnityEngine.Debug.Log("6");
    }

    private void Send(Socket client, String data)
    {
        // Convert the string data to byte data using ASCII encoding.
        byte[] byteData = Encoding.ASCII.GetBytes(data);

        // Begin sending the data to the remote device.
        client.BeginSend(byteData, 0, byteData.Length, 0,
            new AsyncCallback(SendCallback), client);
    }

    private void SendCallback(IAsyncResult ar)
    {
        try
        {
            // Retrieve the socket from the state object.
            Socket client = (Socket)ar.AsyncState;

            // Complete sending the data to the remote device.
            int bytesSent = client.EndSend(ar);
            UnityEngine.Debug.Log("Sent " + bytesSent + " bytes to server.");

            // Signal that all bytes have been sent.
            sendDone.Set();
        }
        catch (Exception e)
        {
            UnityEngine.Debug.Log(e.ToString());
        }
    }
}*/
