﻿using UnityEngine;
using System.Collections;
using Com.LuisPedroFonseca.ProCamera2D;

public class CameraControl : MonoBehaviour {

    public ProCamera2D mainCamera;
    public ProCamera2D IndividualCameraPrefab;
	
    public void FollowTanks(Tank [] tanks)
    {
        foreach (Tank tank in tanks)
        {
            mainCamera.AddCameraTarget(tank.transform);
            ProCamera2D individualCam = GameObject.Instantiate(IndividualCameraPrefab) as ProCamera2D;
            individualCam.AddCameraTarget(tank.transform);
            individualCam.gameObject.SetActive(false);
        }
    }
}
