﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameTimer : MonoBehaviour {

    private static GameTimer instance;
    public static GameTimer Instance
    {
        get
        {
            return instance;
        }
    }

    private bool ticking;
    private float timer;

    public void Stop()
    {
        enabled = false;
    }
    public float Timer
    {
        get
        {
            return timer;
        }

        set
        {
            timer = Mathf.Max(value, 0);
            if (timer > 0) ticking = true;
        }
    }

    public float GetReadyTimer
    {
        get;set;
    }

    public string GetReadyTimerString
    {
        get
        {
            return GetReadyTimer.ToString();
        }
    }

    public string GetTimerString
    {
        get
        {
            return Mathf.FloorToInt(Timer / 60).ToString("00") + ":" + Mathf.FloorToInt(Timer % 60).ToString("00") ;
        }
    }

    public bool IsOver
    {
        get
        {
            return Timer <= 0&&ticking;
        }
    }

    private void Awake()
    {
        GetReadyTimer = 7;
        instance = this;
    }

    void Start()
    {
        enabled = false;
    }

    public void GetReady()
    {
        ServerController.Instance.BroadCastMessage("Get Ready");
        StartCoroutine(StartAfter(GetReadyTimer));
    }

    IEnumerator StartAfter(float time)
    {
        yield return new WaitForSeconds(time);
        enabled = true;
        ServerController.Instance.BroadCastMessage("Start " + GameController.Instance.Round);
    }

    private void Update()
    {
        Timer = Mathf.Max(Timer - Time.deltaTime, 0);
        if (Timer <= 0)
        {
            ServerController.Instance.BroadCastMessage("Time is Over");
            enabled = false;
        }
    }
}
