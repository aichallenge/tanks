﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using System;
using System.Threading;

public class CommunicationLogger {

    FileStream stream;
    DateTime lastLog;

    float flushTime = 1000;
    const string fixedStyle = " style=\"color:_COLOR_;background-color:_BACKGROUND_;font-size:_FONT_SIZE_;\"";
    string color = "#0";
    string backgroundColor = "transparent";
    string fontSize = "12px";

    string tempColor = "";
    string tempBackgroundColor = "";
    string tempFontSize = "";

    bool shouldCloseBold;
    bool shouldCloseItalic;


    public CommunicationLogger(string path, string fileName)
    {
        string style = @"
            <style>
                body {
                    background-color: #FFFFFF;
                }

                div {
                    color: " + color + "; font-size:" + fontSize + ";" + @"
                }
            </style>
        ";

        path += "/" + fileName + "_" + DateTime.Now.ToString("dd_H-mm-ss") + ".html";
        stream = new FileStream(path, FileMode.Create);

        Write("<!DOCTYPE html><html><head><title>"+ fileName + "<meta charset=\"UTF - 8\"></title>" + style + "</head><body>");

        LogTimeStamp();
        StartParagraph().Write("Starting #" + fileName + "# Log at " + path).CloseParagraph();
        Header("Logging");
        DivisorLine();
        Write("<div>");
    }

    ~CommunicationLogger()
    {
        Close();
    }

    public void Close()
    {
        Debug.Log("CLOSED!");
        Write("</div>");
        Write("<p><hr><br><br><i>File Closed</i><hr><hr><br></p>.</body></html>");
        stream.Flush();
        stream.Close();
    }

    string GetStyle()
    {
        return fixedStyle.Replace("_COLOR_", tempColor).Replace("_BACKGROUND_", tempBackgroundColor).Replace("_FONT_SIZE_", tempFontSize);
    }

    void ClearTempStyle ()
    {
        tempColor = color;
        tempBackgroundColor = backgroundColor;
        tempFontSize = fontSize;
    }

    void CloseBoldAndItalicIfNeeded()
    {
        if (shouldCloseBold)
        {
            Write("</b>");
            shouldCloseBold = false;
        }

        if (shouldCloseItalic)
        {
            Write("</i>");
            shouldCloseItalic = false;
        }
    }

    public void Flush()
    {
        stream.Flush();
    }

    public CommunicationLogger SetNextStyleColor(Color color)
    {
        this.tempColor = "#"+ Util.ColorToHex(color);
        return this;
    }

    public CommunicationLogger SetNextStyleBackgroundColor(Color color)
    {
        this.tempBackgroundColor = "#"+Util.ColorToHex(color);
        return this;
    }

    public CommunicationLogger SetNextStyleFontSize(int sizepx)
    {
        this.tempFontSize = sizepx + "px";
        return this;
    }

    public void SetStyle(Color color, Color backgroundColor, int sizepx)
    {
        this.color = Util.ColorToHex(color);
        this.backgroundColor = Util.ColorToHex(backgroundColor);
        this.fontSize = sizepx + "px";
    }

    public CommunicationLogger Header(string header, int n = 1)
    {
        Write("<h" + n + GetStyle() + ">" + header + "</h" + Mathf.Clamp(n, 1, 9) + ">");
        ClearTempStyle();
        CloseBoldAndItalicIfNeeded();
        return this;
    }

    public CommunicationLogger StartParagraph()
    {
        Write("<p" + GetStyle() + ">");
        ClearTempStyle();
        return this;
    }

    public CommunicationLogger CloseParagraph()
    {
        Write("</p>");
        return this;
    }

    public CommunicationLogger DivisorLine()
    {
        Write("<hr>");
        return this;
    }

    public CommunicationLogger NextLogBold()
    {
        Write("<b>");
        shouldCloseBold = true;
        return this;
    }

    public CommunicationLogger NextLogItalic()
    {
        Write("<i>");
        shouldCloseItalic = true;
        return this;
    }

    public CommunicationLogger BreakLine()
    {
        Write("<br>");
        return this;
    }

    public CommunicationLogger LogTimeStamp()
    {
        Write("<p><span" + GetStyle() + ">" + TimeStamp() + "</span></p>");
        ClearTempStyle();
        CloseBoldAndItalicIfNeeded();
        return this;
    }

    public CommunicationLogger LogRawTimeStamp()
    {
        Write(TimeStamp());
        return this;
    }

    public CommunicationLogger LogRawTimeStampMessage(string message)
    {
        Write("<b>" + TimeStamp() + "</b>: " + message + "</br>");
        return this;
    }

    public CommunicationLogger LogTimeStampMessage(string message)
    {
        Write("<b>" + TimeStamp() + "</b>: <span " + GetStyle() + ">" + message + "</span>");
        ClearTempStyle();
        CloseBoldAndItalicIfNeeded();
        return this;
    }

    public CommunicationLogger LogMessage(string message)
    {
        Write("<span "+ GetStyle() + ">" + message);
        ClearTempStyle();
        CloseBoldAndItalicIfNeeded();
        return this;
    }

    public CommunicationLogger Write(string line)
    {
        byte[] encoded = Encoding.UTF8.GetBytes(line);
        DateTime now = DateTime.Now;
        stream.Write(encoded, 0, encoded.Length);

        if ((now - lastLog).Milliseconds >= flushTime)
        {
            stream.Flush();
        }
        lastLog = now;
            
        return this;
    }

    private string TimeStamp()
    {
        return "[" + DateTime.Now.ToString("h:mm:ss-ffff") + "]";
    }
}
