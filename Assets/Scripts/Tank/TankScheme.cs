﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[CreateAssetMenu(fileName = "TankScheme", menuName = "Tank/TankScheme", order = 1)]
public class TankScheme : ScriptableObject {
    public string tankNameDescription;
    public Sprite bodySprite;
    public Sprite barrelSprite;
    public Sprite bulletSprite;

    public Sprite bodySpriteOutline;
    public Sprite barrelSpriteOutline;
    public Sprite bulletSpriteOutline;

    public string tankName;
    public Color tankColor;

    public Sprite UICheckMark;
    public Sprite UIPanel;
    public Sprite UIEdgePanel;

    public string schemeName;
}
