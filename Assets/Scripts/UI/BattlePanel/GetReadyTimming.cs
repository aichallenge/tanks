﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GetReadyTimming : MonoBehaviour {

    public Text label;
    public GameTimer timer;

    private float timeToWait = 7;
    private int getReady = 3;

    private void Start()
    {
        timeToWait = timer.GetReadyTimer;
        StartCoroutine(GetReadyCountDown());
    }

    IEnumerator GetReadyCountDown()
    {
        if (getReady == 0)
        {
            label.text = "Round " + GameController.Instance.Round.ToString();
            yield return new WaitForSeconds(2);
            label.text = "Go!";
            yield return new WaitForSeconds(2);
            Destroy(gameObject);
            yield break;
        }

        label.text = getReady.ToString();
        yield return new WaitForSeconds(1);
        getReady--;
        StartCoroutine(GetReadyCountDown());
    }
}
