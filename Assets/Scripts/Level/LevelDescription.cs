﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class LevelDescription
{
    [Serializable]
    public class LevelProperties
    {
        public string LevelSetting;
    }

    [Serializable]
    public class LayerProperties
    {
    }

    [Serializable]
    public class LayerObjectProperties
    {
    }

    [Serializable]
    public class TileSetProperty
    {
        public string id = "";
        public string tileTag = "Untagged";
        public bool collider = false;
    }

    [Serializable]
    public class LayerObject
    {
        public int gid = -1;
        public int height;
        public int id = -1;
        public string name;
        public float rotation;
        public string type;
        public bool visible;
        public int width;
        public float x;
        public float y;

        public LayerObjectProperties properties;
    }

    [Serializable]
    public class Layer
    {
        public int [] data;
        public int height;
        public int width;
        public float x;
        public float y;
        public bool visible;
        public string type;
        public float opacity;
        public string name;
        public string draworder;
        public LayerObject [] objects;

        public LayerProperties properties;
    }

    [Serializable]
    public class Tile
    {
        public string id;
        public string image;
    }

    [Serializable]
    public class TileSet
    {
        public int columns;
        public int firstgid;
        public int margin;
        public string name;
        public int spacing;
        public int tilecount;
        public int tileheight;
        public int tilewidth;

        //these are dictionary at Tiled generated json file. So it is not supported by Unity Json Serializer.
        //you must set this up manually for now. An automatic routine should be prepared in the future.
        //Basically, you transform the property and tiles in a list. The key should become a property 'id'. So before was:
        // { "0" : {}, "1" : {} } 
        //And now:
        // [ {"id":"0"}, {"id":"1"} ]
        //sure, tileproperties must has a index for every tile. 
        public Tile [] tiles;
        public TileSetProperty [] tileproperties;
    }

    public int nextobjectid;
    public string orientation;
    public string renderorder;
    public int tileheight;
    public int tilewidth;
    public float version;
    public int width;
    public int height;

    public Layer[] layers;
    public TileSet[] tilesets;
    public LevelProperties properties;

    public static LevelDescription Load(string path)
    {
        string json = System.IO.File.ReadAllText(path);
        //adjust json so the dictionary becomes a list and than it can be interpreted by default Unity's json serializer
        json = TransformJsonDictionary(json, "\"tiles\"");
        json = TransformJsonDictionary(json, "\"tileproperties\"");
        return UnityEngine.JsonUtility.FromJson<LevelDescription>(json);
    }

    static string TransformJsonDictionary(string json, string propertyToTransform)
    {
        int tilesIndex = json.IndexOf(propertyToTransform);

        while (tilesIndex != -1)
        {
            tilesIndex += propertyToTransform.Length;

            System.Text.StringBuilder jsonBuilder = new System.Text.StringBuilder(json);

            int toReplace = json.IndexOf("{", tilesIndex);
            jsonBuilder[toReplace] = '[';
            tilesIndex = toReplace;
            int lastValidTileIndex = tilesIndex;
            do
            {
                int keyIndex = json.IndexOf("\"", tilesIndex);
                int keyEndIndex = json.IndexOf(":", keyIndex);
                string key = json.Substring(keyIndex, keyEndIndex - keyIndex);
                int valueIndex = json.IndexOf("{", tilesIndex + 1);

                jsonBuilder.Insert(valueIndex + 1, "\"id\":" + key + ",");
                jsonBuilder.Remove(keyIndex, keyEndIndex - keyIndex + 1);

                json = jsonBuilder.ToString();

                tilesIndex = valueIndex;
                lastValidTileIndex = tilesIndex;

                for (; tilesIndex < json.Length; ++tilesIndex)
                {
                    if (json[tilesIndex] == '}')
                    {
                        lastValidTileIndex = tilesIndex + 1;
                        if (json[tilesIndex + 1] == ',')
                        {
                            tilesIndex = tilesIndex + 1;
                            break;
                        }
                        else
                        {
                            tilesIndex = -1;
                            break;
                        }
                    }
                }

                jsonBuilder = new System.Text.StringBuilder(json);

            } while (tilesIndex >= 0 && tilesIndex < json.Length - 2);

            tilesIndex = json.IndexOf("}", lastValidTileIndex);
            jsonBuilder[tilesIndex] = ']';
            json = jsonBuilder.ToString();

            tilesIndex = json.IndexOf(propertyToTransform, tilesIndex);
        }

        return json;
    }
}
