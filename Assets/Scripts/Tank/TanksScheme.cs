﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "TanksScheme", menuName = "Tank/SchemeSet", order = 1)] 
public class TanksScheme : ScriptableObject {
    public TankScheme[] tanks;
}
