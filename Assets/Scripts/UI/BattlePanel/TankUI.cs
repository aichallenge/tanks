﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TankUI : MonoBehaviour {

    public Text tankName;
    public Image tankBody;
    public Image tankBarrel;
    public Text points;

    protected Tank tank;

    public virtual void LinkTank(Tank tank)
    {
        this.tank = tank;

        if (tankName  != null)
            tankName.text = tank.TankScheme.tankName;

        if (tankBarrel != null)
            tankBarrel.sprite = tank.TankScheme.barrelSpriteOutline;

        if (tankBody != null)
            tankBody.sprite = tank.TankScheme.bodySpriteOutline;

        if (points != null)
        points.text = tank.Score.ToString("0000");
    }
	
	virtual protected void Update () {
        if (tank == null) return;
        if (tankName != null)
            tankName.text = tank.TankScheme.tankName;

        if (points != null)
            points.text = tank.Score.ToString("0000");
    }

    private void OnDestroy()
    {
        tank = null;
    }
}
