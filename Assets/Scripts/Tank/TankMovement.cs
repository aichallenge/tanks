﻿using UnityEngine;
using System.Collections;

public class TankMovement : MonoBehaviour {


    public enum TankStopReason
    {
        HitObject,
        Command
    }

    public bool IsMoving
    {
        get
        {
            return rigid.velocity.x != 0 || rigid.velocity.y != 0;
        }
    }

    public Vector2 Velocty
    {
        get
        {
            return rigid.velocity;
        }
    }

    /// <summary>
    /// Tank finished it's animation and started to move. 
    /// Started at position with velocity
    /// </summary>
    public System.Action<TankMovement, Vector2, Vector2> StartedMovement;

    /// <summary>
    /// Tank has stopped for some motive at position
    /// If it was a command, GameObject shall be null. It will contain the object it hit otherwise
    /// </summary>
    public System.Action<TankMovement, Vector2, GameObject> StoppedMovement;

    [SerializeField]
    private Rigidbody2D rigid;

    [SerializeField]
    private Transform body;

    [SerializeField]
    private float speed;

    private Vector2 directionToMove;

    private TankStopReason stopReason;

    //For animation

    [SerializeField]
    private float timeToAdjust = 0.5f;

    private bool adjustBodyDirection = false;
    private Quaternion currentOrientation;
    private Quaternion targetOrientationDegree;
    private float adjustBodyAnimationTimer = 0;

    void Awake()
    {
        if (rigid == null)
            rigid = GetComponent<Rigidbody2D>();
    }

    public void Move(float angle)
    {
        directionToMove = Util.VectorFromRadianAngle(angle).normalized * speed;

        currentOrientation = body.rotation;
        targetOrientationDegree = Quaternion.Euler(new Vector3(0, 0, angle * Mathf.Rad2Deg));
        rigid.velocity = Vector2.zero;
        adjustBodyDirection = true;
        adjustBodyAnimationTimer = 0;
    }

    public void Stop(GameObject hit = null)
    {
        adjustBodyDirection = false;
        directionToMove = rigid.velocity = Vector2.zero;

        if (StoppedMovement != null)
        {
            StoppedMovement(this, new Vector2(transform.position.x, transform.position.y), hit);
        }
    }
    
    void Update()
    {
        if (adjustBodyDirection)
        {
            adjustBodyAnimationTimer = Mathf.Min(adjustBodyAnimationTimer + Time.deltaTime, timeToAdjust);
            body.rotation = Quaternion.Lerp(currentOrientation, targetOrientationDegree, adjustBodyAnimationTimer / timeToAdjust);
            if (adjustBodyAnimationTimer >= timeToAdjust)
            {
                adjustBodyDirection = false;
                rigid.velocity = directionToMove;
                if (StartedMovement != null)
                {
                    StartedMovement(this, new Vector2(transform.position.x, transform.position.y), directionToMove);
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        this.Stop(collision.transform.gameObject);
        Rigidbody2D r = GetComponent<Rigidbody2D>();
        r.MovePosition(r.position + collision.contacts[0].normal.normalized / 50); // move enough so it can detect a collision enter, and detect a paralel movement
    }

    private void OnValidate()
    {
        if (rigid == null)
            rigid = GetComponent<Rigidbody2D>();
    }
}
