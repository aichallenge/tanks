﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Com.LuisPedroFonseca.ProCamera2D;

public class LevelAutoLoader : MonoBehaviour {

    public CameraControl cameraControl;

    public static string levelPath = "";
    public string testingLevel = "";

    public TankStatsPanelControl tankStatsPanelControl;

    [System.Serializable]
    private struct LevelInfo
    {
        public int columns;
        public int[] data;
        public int rounds;
        public int kills;
        public float time;
    }
	
	IEnumerator Start () {
        LevelLoader loader = new LevelLoader();
        if (levelPath == "")
        {
            if (testingLevel == "")
            {
                yield break;
            }

            Debug.Log("Using test tank");
            loader.usingTestTank = true;
            levelPath = testingLevel;
        }
        Debug.Log("Loading level " + levelPath);
        yield return new WaitForSeconds(0.5f);
        List<Tank> tanks = loader.LoadFromFile(levelPath);
        GameController.Instance.Tanks = tanks.ToArray();

        tankStatsPanelControl.ShowTankStats(GameController.Instance.Tanks);

        GameController.Instance.gameInfo.KillPerRoundToVictory = Mathf.Min(GameController.Instance.gameInfo.KillPerRoundToVictory, tanks.Count - 1);
        GameController.Instance.Round++;
        GameController.Instance.enabled = true;

        GameTimer.Instance.Timer = 120;

        LevelInfo levelInfo = new LevelInfo();
        levelInfo.columns = loader.Columns;
        levelInfo.data = loader.LowLevelDescription();
        levelInfo.rounds = GameController.Instance.gameInfo.Rounds;
        levelInfo.kills = GameController.Instance.gameInfo.KillPerRoundToVictory;
        levelInfo.time = GameTimer.Instance.Timer;
        ServerController.Instance.BroadCastMessage("{\"level\":" + JsonUtility.ToJson(levelInfo) + "}");
        levelInfo.data = null;

        cameraControl.FollowTanks(tanks.ToArray());

        GameTimer.Instance.GetReady();
    }

    private void OnDestroy()
    {
        Tank.CleanAvaliableTanks();
    }
}
