﻿using UnityEngine;
using System.Collections;

public class ActiveByKey : MonoBehaviour {

    public KeyCode key;
    public GameObject go;
	
	void Update () {
	    if (Input.GetKeyDown(key))
        {
            go.SetActive(!go.activeSelf);
        }
	}
}
