﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TankRoundUI : TankUI {

    public RectTransform VictoryBulletsParent;
    public RectTransform victoryPatternPrefab;

    private List<Image> roundBullets = new List<Image>();

    public override void LinkTank(Tank tank)
    {
        base.LinkTank(tank);
        foreach(Transform t in VictoryBulletsParent)
        {
            Destroy(t.gameObject);
        }

        for (int i = 0; i < GameController.Instance.gameInfo.KillPerRoundToVictory; ++i)
        {
            RectTransform bullet = GameObject.Instantiate(victoryPatternPrefab) as RectTransform;
            bullet.name = bullet.name.Replace("(Clone)", "");
            bullet.transform.SetParent(VictoryBulletsParent.transform, false);
            Image bulletImage = bullet.transform.Find("Bullet").GetComponent<Image>();
            bulletImage.sprite = tank.TankScheme.bulletSpriteOutline;
            bulletImage.enabled = false;
            roundBullets.Add(bulletImage);
        }
    }

    override protected void Update()
    {
        base.Update();

        int roundKills = tank.Kills;
        for (int i = 0; i < roundKills; ++i)
        {
            roundBullets[i].enabled = true;
        }
    }
}
