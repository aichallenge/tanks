﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ServerController : MonoBehaviour {

    private ServerSync serverSync;

    private static ServerController instance;
    public static ServerController Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject go = new GameObject("ServerController");
                instance = go.AddComponent<ServerController>();
                DontDestroyOnLoad(go);
            }

            return instance;
        }
    }


    #region Logging stuff
    public bool Logging
    {
        get
        {
            return logging;
        }

        set
        {
            if (value && logger == null)
            {
                CreateLogger();
            }

            logging = value;
        }
    }

    private CommunicationLogger logger;
    private bool logging = true;
    private Color[] logColors = { Color.magenta, Color.blue, Color.red, Color.cyan };

    private void CreateLogger()
    {
        logger = new CommunicationLogger("Logs", "server_log");
        logger.NextLogBold().SetNextStyleColor(Color.red).LogTimeStampMessage("Accepting Clients").BreakLine().DivisorLine();
    }

    private void Log(string message, int id = -1)
    {
        if (logger != null && logging)
        {
            if (id >= 0)
            {
                logger.SetNextStyleColor(logColors[id % logColors.Length]).LogTimeStampMessage(message).BreakLine();
            }
            else
            {
                logger.LogRawTimeStampMessage(message);
            }
        }
    }

    public void FlushLog(int tocomplainsignaturei=0, string tocomplainsignature="")
    {
        if (logger != null)
        {
            logger.Flush();
        }
    }
    #endregion



    public class TankClient
    {
        public bool IsReady { get; set; }
        public string NameId
        {
            get
            {
                return scheme.tankNameDescription;
            }
        }

        public string Name
        {
            get
            {
                return name;
            } 

            set
            {
                name = value;
            }
        }

        public Color Color
        {
            get
            {
                return color;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }
        }

        public TankScheme Scheme
        {
            get
            {
                return scheme;
            }
        }

        private string name;
        private Color color;
        private int id;
        private TankScheme scheme;

        public TankClient (int id, TankScheme tankScheme)
        {
            this.id = id;
            this.name = tankScheme.tankName;
            this.color = tankScheme.tankColor;
            this.scheme = tankScheme;

            ServerController.instance.messageReceived += messageReceived;
        }

        ~TankClient()
        {
            scheme = null;
            if (ServerController.instance != null) ServerController.instance.messageReceived -= messageReceived;
        }

        private void messageReceived(int arg1, string arg2)
        {
            if (arg1 == this.id && MessageReceived != null) MessageReceived(this, arg2);
        }

        public void SendMessage(string message)
        {
            ServerController.instance.SendMessageToClient(this.id, message);
            if (MessageSended != null) MessageSended(this, message);
        }

        public event System.Action<TankClient, string> MessageReceived;
        public event System.Action<TankClient, string> MessageSended;
    }


    public List<TankClient> Clients
    {
        get
        {
            return clients;
        }
    }

    private List<TankClient> clients = new List<TankClient>();
    private Dictionary<string, System.Action<int, string>> publicGameCommands = new Dictionary<string, System.Action<int, string>>();
    private event System.Action<int, string> messageReceived;
    public TanksScheme tanksScheme;
    public event System.Action<TankClient> TankClientConnected;
    private int nextTankScheme = 0;

    public string Ip
    {
        get
        {
            return serverSync.Ip;
        }
    }

    public int Port
    {
        get
        {
            return serverSync.Port;
        }
    }

    void Awake () {
        if (instance != null)
        {
            Debug.LogWarning("Another instance already exist");
            Destroy(gameObject);
            return;
        }

        instance = this;

        if (tanksScheme == null)
            tanksScheme = Resources.Load<TanksScheme>("TanksScheme");

        Logging = logging;

        serverSync = ServerSync.Instance;
        serverSync.loadPortAndIpFromJsonOnStart = true;
        serverSync.LoadIpFromJson();

        serverSync.ClientConnected += ClientConnected;
        serverSync.MessageReceived += MessageReceived;

        PopulatePublicCommands();
    }

    void OnDestroy()
    {
        if (logger != null)
        {
            logger.LogTimeStampMessage("Closing connection... ").DivisorLine();
            logger.Close();
        }
    }

    private TankClient FindTank(int id)
    {
        return clients.Find(x => { return x.Id == id; });
    }

    private void PopulatePublicCommands()
    {
        publicGameCommands.Add("flush", FlushLog);
        publicGameCommands.Add("echo", SendMessageToClient);
        publicGameCommands.Add("setName", SetTankName);
        publicGameCommands.Add("ready", TankReady);
    }

    private void TankReady(int id, string name)
    {
        TankClient tank = clients.Find(x => { return x.Id == id; });
        if (tank != null)
        {
            tank.IsReady = true;
            tank.SendMessage("You are Ready");
        }

        bool allReady = true;
        foreach(TankClient client in clients)
        {
            allReady = client.IsReady && allReady;
        }
        if (allReady)
        {
            BroadCastMessage("Everyone is Ready");
        }
    }

    private void SetTankName(int id, string name)
    {
        TankClient tank = clients.Find(x => { return x.Id == id; });
        if (tank != null)
        {
            tank.Name = name;
            tank.SendMessage("NewName: " + tank.Name);
        }
    }

    private void ProcessPublicCommand(int id, string message)
    {
        int endOfCommand = message.IndexOf(" ");
        if (endOfCommand < 0) return;
        string command = message.Substring(1, endOfCommand - 1);
        System.Action<int, string> callback = null;
        if (publicGameCommands.TryGetValue(command, out callback))
        {
            callback(id, message.Substring(endOfCommand, message.Length - endOfCommand));
        } else
        {
            Log("command not recognized: " + message, id);
        }
    }

    private void ClientConnected(int clientId)
    {
        Debug.Log("Client Connected: <color=" + "#" + Util.ColorToHex(logColors[clientId % logColors.Length]) + ">[" + clientId + "]</color>");
        if (logger != null)
        {
            logger.SetNextStyleColor(Color.green).LogTimeStampMessage("Client Connected #").NextLogBold().SetNextStyleColor(logColors[clientId%logColors.Length]).LogMessage(clientId.ToString("00")).BreakLine().DivisorLine().BreakLine();
        }

        if (nextTankScheme >= tanksScheme.tanks.Length)
        {
            Debug.LogError("No more tanks avaliable");
            return;
        }
        TankClient newClient = new TankClient(clientId, tanksScheme.tanks[nextTankScheme++]);
        clients.Add(newClient);

        string welcome = "{";
            welcome += "\"color\":" + JsonUtility.ToJson(newClient.Color) + ",";
            welcome += "\"name\":\"" + newClient.Name + "\",";
            welcome += "\"id\":" + newClient.Id + ",";
            welcome += "\"nameId\":\"" + newClient.NameId + "\",";
            welcome += "\"boxTankSize\":" + Util.SizeToJson(0.6904119f, 0.722946f) + ",";
            welcome += "\"boxTankOffset\":" + Util.PositionToJson(0.01773651f, 0.005182117f) + ",";
            welcome += "\"bodySpriteSize\":" + Util.SizeToJson(70, 75);
        welcome += "}";

        Debug.Log(welcome);
        newClient.SendMessage(welcome);

        if (TankClientConnected != null)
        {
            TankClientConnected(newClient);
        }
    }

    private void MessageReceived(int clientId, string message)
    {
        Debug.Log(" << <color=" + "#" + Util.ColorToHex(logColors[clientId % logColors.Length]) + ">" + "[" + clientId + "]</color>:<b>" + message + "</b>");
        Log(" << " + message, clientId);

        if (message[0] == '/')
        {
            ProcessPublicCommand(clientId, message);
            return;
        }

        if (messageReceived != null) messageReceived(clientId, message);
    }

    private void SendMessageToClient(int id, string message)
    {
        message += "\n";
        Debug.Log(" >> <color=" + "#" + Util.ColorToHex(logColors[id % logColors.Length]) + ">" + "[" + id + "]</color>" + ": <b>" + message + "</b>");
        Log(" >> <b>" + message + "</b>", id);

        serverSync.SendMessageToClient(message, id);
    }

    public void BroadCastMessage(string message)
    {
        Debug.Log("Sending to **ALL** : <b>" + message + "</b>");
        Log(" *>> <b>" + message + "</b>");

        serverSync.BroadCastMessage(message);
    }

    void Update()
    {
    }
}
