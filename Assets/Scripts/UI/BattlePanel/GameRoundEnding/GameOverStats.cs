﻿using UnityEngine;
using System.Collections;

public class GameOverStats : MonoBehaviour {

    public GameOverRow[] rows;

    public void Show(Tank []tanks)
    {
        int i = 0;
        for (; i < tanks.Length; ++i)
        {
            rows[i].Show(tanks[i]);
        }

        for (;i<rows.Length; ++i)
        {
            rows[i].gameObject.SetActive(false);
        }
    }
}
