﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;


internal class Server
{

    #region Delegates

    /// <summary>
    /// To listen any message received by client.
    /// It calls with internal client id and the message as a string
    /// </summary>
    public System.Action<int, string> ReceivedClientMessage;

    /// <summary>
    /// To listen when a client connects.
    /// Calls with internal client id.
    /// </summary>
    public System.Action<int> ClientConnected;

    #endregion


    #region Public Property

    /// <summary>
    /// Limits the number of connected Client
    /// </summary>
    public int AcceptClientCount
    {
        get
        {
            return acceptClientCount;
        }

        set
        {
            acceptClientCount = value;
        }
    }

    #endregion


    #region Private Fields

    private bool acceptingClients = true;
    private int acceptClientCount = 10;

    private string server = "127.0.0.1";
    private int port = 5000;

    private List<Client> clients = new List<Client>();
    private Thread acceptingClientsThread;
    private TcpListener listener;

    #endregion


    public Server(string ip, int port)
    {
        this.port = port;
        this.server = ip;
    }


    #region Public Methods

    public void Start()
    {
        clients.Clear();
        acceptingClients = true;

        acceptingClientsThread = new Thread(this.StartAcceptingClients);
        acceptingClientsThread.Start();
    }

    #endregion


    #region Private Methods

    private void StartAcceptingClients()
    {
        IPAddress localAdd = IPAddress.Parse(server);
        listener = new TcpListener(localAdd, port);
        listener.Start();

        while (acceptingClients)
        {
            TcpClient client = null;
            client = listener.AcceptTcpClient();

            if (client != null)
            {
                AcceptClient(client);
            }

            Thread.Sleep(1);
        }
    }

    private void AcceptClient(TcpClient client)
    {
        Debug.Log("<color=blue>Client Accepted... </color>");
        Client newClient = new Client(client);

        newClient.Start(ReceveidMessageCallback);

        lock (clients)
        {
            clients.Add(newClient);
        }

        if (ClientConnected != null)
        {
            ClientConnected(newClient.Id);
        }
    }

    private void ReceveidMessageCallback(int id, string message)
    {
        if (ReceivedClientMessage != null)
        {
            ReceivedClientMessage(id, message);
        }
    }

    #endregion


    #region Public Methods that needed to be sync with main thread

    /// <summary>
    /// In this region are methods that shall be called in main thread. To prevent then to block UI they are called within a thread.
    /// So they lock a variable inside a thread letting main thread to be free.
    /// </summary>

    #region Public methods

    public void Close()
    {
        Thread t = new Thread(() => { _Close(); });
        t.Start();
    }

    public void Close(int id)
    {
        Thread t = new Thread(() => { _Close(id); });
        t.Start();
    }

    public void BroadCastMessage(string message)
    {
        Thread t = new Thread(() => { _BroadCastMessage(message); });
        t.Start();
    }

    public void SendMessage(string message, int id)
    {
        Thread t = new Thread(() => { _SendMessage(message, id); });
        t.Start();
    }

    #endregion


    #region Private methods
    private void _Close(int id)
    {
        Client client = null;
        lock (clients)
        {
            client = clients.Find(x => { return x.Id == id; });
            clients.Remove(client);
        }

        if (client != null)
            client.Close();
    }

    private void _Close()
    {
        acceptingClients = false;

        listener.Stop();
        acceptingClientsThread.Abort();

        lock (clients)
        {
            foreach (Client client in clients)
            {
                client.Close();
            }

            clients.Clear();
        }

        Debug.Log("<color=blue>Server Closed</color>");
    }

    private void _BroadCastMessage(string message)
    {
        lock (clients)
        {
            foreach (Client client in clients)
            {
                client.AddMessage(message);
            }
        }
    }

    private void _SendMessage(string message, int id)
    {
        Client client = null;
        lock (clients)
        {
            client = clients.Find(x => { return x.Id == id; });
        }

        if (client != null)
            client.AddMessage(message);
    }

    #endregion
    #endregion

    
    #region Private Class Client 

    private class Client
    {
        private static int _id;

        private List<string> messagesToSend = new List<string>();
        private NetworkStream nwStream;
        private Thread writeThread;
        private Thread readThread;
        private TcpClient client;
        public bool reading;
        public bool writing;
        private int id;
        private byte[] buffer;

        private System.Action<int, string> receivedMessageCallback;


        public Client(TcpClient client)
        {
            this.client = client;
            nwStream = client.GetStream();
            id = _id++;
            reading = true;
            writing = true;
            buffer = new byte[client.ReceiveBufferSize];
        }

        public int Id
        {
            get
            {
                return id;
            }
        }

        public Thread WriteThread
        {
            get
            {
                return writeThread;
            }
            set
            {
                writeThread = value;
            }
        }

        public Thread ReadThread
        {
            get
            {
                return readThread;
            }
            set
            {
                readThread = value;
            }
        }

        public void AddMessage(string message)
        {
            lock (messagesToSend)
            {
                messagesToSend.Add(message);
            }
        }

        public bool HasMessage
        {
            get
            {
                lock (messagesToSend)
                {
                    return messagesToSend.Count > 0;
                }
            }
        }

        public void SendMessages()
        {
            lock (messagesToSend)
            {
                foreach (string message in messagesToSend)
                {
                    byte[] bytes = Encoding.UTF8.GetBytes(message);
                    nwStream.Write(bytes, 0, bytes.Length);
                }

                messagesToSend.Clear();
            }
        }

        public string ReceiveMessage()
        {
            int bytesRead = 0;
            bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);
            if (bytesRead <= 0) return "";
            return Encoding.UTF8.GetString(buffer, 0, bytesRead);
        }

        public void Close()
        {
            reading = false;
            writing = false;
            messagesToSend.Clear();
            messagesToSend = null;
            if (readThread != null) readThread.Abort();
            readThread = null;
            if (writeThread != null) writeThread.Abort();
            writeThread = null;
            client.Close();
        }

        public void Start(System.Action<int, string> receiveMessageCallback)
        {
            this.receivedMessageCallback = receiveMessageCallback;
            this.readThread = new Thread(this.Read);
            this.readThread.Start();

            this.writeThread = new Thread(this.Write);
            this.writeThread.Start();
        }

        private void Read()
        {
            while (reading)
            {
                string dataReceived = ReceiveMessage();
                if (dataReceived == "")
                {
                    Thread.Sleep(1);
                    continue;
                }
                if (receivedMessageCallback != null) receivedMessageCallback(Id, dataReceived);
                Thread.Sleep(1);
            }
        }

        private void Write()
        {
            while (writing)
            {
                if (HasMessage)
                {
                    SendMessages();
                    Thread.Sleep(1);
                }
                else
                {
                    Thread.Sleep(1);
                }
            }
        }
    }

    #endregion

}