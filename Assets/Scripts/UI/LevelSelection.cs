﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using UnityEngine.SceneManagement;

public class LevelSelection : MonoBehaviour {

    public RectTransform panelList;
    public Button selectLevelRow;
    public Button selectAndContinueButton;
    public string pathToFindLevel;
    public Sprite highlight;

    private Sprite normalSprite;
    private Button currentSelectedButton;
    private string selectedLevel = "";

	void Start () {
        selectAndContinueButton.interactable = false;
        string[] files = Directory.GetFiles(pathToFindLevel, "*.json");
        foreach(string file in files)
        {
            Button row = GameObject.Instantiate(selectLevelRow) as Button;
            row.transform.SetParent(panelList.transform, false);
            row.GetComponentInChildren<Text>().text = Path.GetFileNameWithoutExtension(file);
            string localFile = file;
            row.onClick.AddListener(() =>
            {
                selectAndContinueButton.interactable = true;
                if (currentSelectedButton!= null)
                {
                    currentSelectedButton.GetComponent<Image>().sprite = normalSprite;
                }
                currentSelectedButton = row;
                normalSprite = row.GetComponent<Image>().sprite;
                row.GetComponent<Image>().sprite = highlight;
                selectedLevel = localFile;
                Debug.Log("File: " + localFile);
            });
        }
	}

    public void Play()
    {
        GameController.Instance.Restart(Path.GetFileNameWithoutExtension(selectedLevel));
        LevelAutoLoader.levelPath = selectedLevel;
        Debug.Log("Loading Level: " + selectedLevel);
        SceneManager.LoadScene("TankBattle");
    }
}
