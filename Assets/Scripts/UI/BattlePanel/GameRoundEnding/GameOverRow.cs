﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverRow : MonoBehaviour {

    public Image RowBackground;
    public Text tankName;
    public Text tankScore;
    public Text tankKills;
    public Text tankDeaths;
	
    public void Show(Tank tank)
    {
        if (tank == null)
        {
            RowBackground.gameObject.SetActive(false);
            tankName.gameObject.SetActive(false);
            tankScore.gameObject.SetActive(false);
            tankKills.gameObject.SetActive(false);
            tankDeaths.gameObject.SetActive(false);

            return;
        }

        GameScoreControl.TankScore score = GameController.Instance.scoreControl.TotalScoreFor(tank);
        tankName.text = tank.TankScheme.tankName;
        RowBackground.sprite = tank.TankScheme.UIEdgePanel;

        tankKills.text = score.kills.ToString();
        tankDeaths.text = score.deaths.ToString();
        tankScore.text = score.score.ToString();
    }
}
