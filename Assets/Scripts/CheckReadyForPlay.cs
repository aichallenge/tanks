﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CheckReadyForPlay : MonoBehaviour {

    public Button ReadyButton;

    void Update()
    {
        List<ServerController.TankClient> clients = ServerController.Instance.Clients;

        bool allReady = clients.Count != 0;
        foreach(ServerController.TankClient tank in clients)
        {
            allReady = allReady && tank.IsReady;
        }

        ReadyButton.interactable = allReady;
    }
}
