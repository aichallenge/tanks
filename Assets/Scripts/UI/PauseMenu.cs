﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {
    private void OnDisable()
    {
        Time.timeScale = 1;
    }

    private void OnEnable()
    {
        Time.timeScale = 0;
    }
}
