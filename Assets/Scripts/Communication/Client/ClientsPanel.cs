﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ClientsPanel : MonoBehaviour {

    [SerializeField]
    private InputField inputField;

    [SerializeField]
    private Text console;

    [SerializeField]
    private int lineLimit = 5;

    private string[] consoleLines;

    private Dictionary<string, ClientSync> clients = new Dictionary<string, ClientSync>();
    private string lastRawMessage = "";

    private void Start()
    {
        ClientSync.Ip = ServerSync.Instance.Ip;
        ClientSync.Port = ServerSync.Instance.Port;


        consoleLines = new string[lineLimit];
        for (int i = 0; i < consoleLines.Length; ++i)
        {
            consoleLines[i] = " >> ";
        }

        console.text = string.Join("\n", consoleLines);

        inputField.onEndEdit.AddListener(val =>
        {
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                ProcessMessage(inputField.text);
                inputField.text = "";
                inputField.ActivateInputField();
                inputField.Select();
            }
        });
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            inputField.text = lastRawMessage;
        }
    }

    private IEnumerator _OnEnable()
    {
        yield return new WaitForEndOfFrame();
        inputField.ActivateInputField();
        inputField.Select();
    }

    private void OnEnable()
    {
        StartCoroutine(_OnEnable());
    }

    public void ProcessMessage()
    {
        ProcessMessage(inputField.text);
        inputField.text = "";
    }

    public void ProcessMessage(string x)
    {
        lastRawMessage = x;
        if (x.IndexOf ("/connect ") == 0)
        {
            x = x.Replace("/connect ", "").Trim();
            ConnectClient(x);
            return;
        }

        if (x.IndexOf ("/disconnect ") == 0)
        {
            x = x.Replace("/disconnect ", "").Trim();
            DisconnectClient(x);
            return;
        }

        string[] options = x.Split(new string[] { ":" }, System.StringSplitOptions.RemoveEmptyEntries);
        if (options.Length >= 2)
        {
            SendMessageAs(options[0].Trim(), options[1].Trim());
        } else {
            AddConsoleMessage("<color=yellow>" + x + "</color>");
        }
    }

    ClientSync GetClient(string user)
    {
        ClientSync client = null;
        clients.TryGetValue(user, out client);
        if (client != null)
        {
            return client;
        }
        else
        {
            AddConsoleMessage("No client named " + user + " was found");
        }

        return null;
    }

    void SendMessageAs(string user, string message)
    {
        ClientSync client = GetClient(user);
        if (client != null)
        {
            client.SendMessageToServer(message);
            AddConsoleMessage("<color=red>[" + user + "]</color> << " + message);
        }
    }

    void ConnectClient(string name)
    {
        AddConsoleMessage("Connecting " + name + " ...");
        ClientSync client = null;
        clients.TryGetValue(name, out client);
        if (client != null)
        {
            AddConsoleMessage("User " + name + " already exists");
            return;
        }

        GameObject go = new GameObject("Client " + name);
        client = go.AddComponent<ClientSync>();
        DontDestroyOnLoad(go);
        clients.Add(name, client);

        string localName = name;
        client.Connected = delegate ()
        {
            AddConsoleMessage("<color=green>Client</color> " + localName + " connected.");
        };

        client.MessageReceived = delegate (string message)
        {
            AddConsoleMessage("<color=blue>[" + localName + "]</color> >> " + message);
        };
    }

    private void OnDestroy()
    {
        foreach (KeyValuePair<string, ClientSync> entry in clients)
        {
            entry.Value.Connected = null;
            entry.Value.MessageReceived = null;
        }

        clients.Clear();
    }

    void DisconnectClient(string name)
    {
        ClientSync client = GetClient(name);
        if (client != null)
        {
            Debug.Log("Should disconnect client " + name + " but method do not exist yet");
            clients.Remove(name);
        }
    }

    void AddConsoleMessage(string message)
    {
        message = message.Trim();
        for (int i = 0; i < consoleLines.Length-1; i++)
        {
            consoleLines[i] = consoleLines[i + 1];
        }
        string consoleOutput = " >> " + message;
        consoleLines[consoleLines.Length-1] = consoleOutput;
        console.text = string.Join("\n", consoleLines);
    }
}
