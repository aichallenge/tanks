﻿using UnityEngine;
using System.Collections;

public static class Util {

    public static string SizeToJson(float w, float h)
    {
        return "{\"width\":" + w + ", \"height\":" + h + "}";
    }

    public static string PositionToJson(float x, float y)
    {
        return "{\"x\":" + x + ", \"y\":" + y + "}";
    }

    public static Vector2 VectorFromRadianAngle(float radian)
    {
        return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
    }

    public static Vector2 VectorFromDegreeAngle(float degree)
    {
        return VectorFromRadianAngle(degree * Mathf.Deg2Rad);
    }

    public static float RadianFromVector(Vector2 vec)
    {
        return Vector2.Angle(vec, Vector2.right);
    }

    public  static string ColorToHex(Color32 color)
    {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }

    public static Color HexToColor(string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        return new Color32(r, g, b, 255);
    }
}
