﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

public class LevelLoader
{
    public bool usingTestTank = false;
    private Dictionary<int, GameObject> tiles = new Dictionary<int, GameObject>();
    private int columns;
    private List<int> lowLevel;

    public List<Tank> LoadFromFile(string path, bool useOnlyConnectedTanks = true)
    {
        return LoadFromDescription(LevelDescription.Load(path), useOnlyConnectedTanks);
    }

    Sprite LoadSprite(string path)
    {
        path = "Level/" + path;
        Texture2D tex = new Texture2D(4, 4);
        if (File.Exists(path))
        {
            tex.LoadImage(File.ReadAllBytes(path));
        } else
        {
            Debug.Log("File do not exist: " + path);
        }
        return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
    }

    public int Columns
    {
        get
        {
            return columns;
        }
    }

    public int [] LowLevelDescription()
    {
        return lowLevel.ToArray();
    }

    private void PrepareTileset(LevelDescription.TileSet tileSet, LevelDescription.TileSetProperty[] properties)
    {
        string tileSetName = tileSet.name;
        int tileHeight = tileSet.tileheight;
        int tileWidth = tileSet.tilewidth;

        for (int i = 0; i < tileSet.tiles.Length; ++i)
        {
            LevelDescription.Tile tile = tileSet.tiles[i];
            LevelDescription.TileSetProperty tileProperty = properties != null ? System.Array.Find<LevelDescription.TileSetProperty>(properties, x => { return x.id == tile.id; }) : null;

            int index = int.Parse(tile.id);

            GameObject go = new GameObject(Path.GetFileName(tile.image));
            SpriteRenderer renderer = go.AddComponent<SpriteRenderer>();
            renderer.sprite = LoadSprite(tile.image);

            if (tileProperty != null)
            {
                go.tag = tileProperty.tileTag;
                if (tileProperty.collider)
                {
                    go.AddComponent<BoxCollider2D>();
                }
            }

            if (renderer.sprite != null)
            {
                tiles.Add(index + tileSet.firstgid, go);
            }
        }
    }

    private void RenderLayerTiles(LevelDescription.Layer layer, GameObject layerGo)
    {
        for (int i = 0; i < layer.width; ++i)
        {
            if (layer.data == null) continue;
            for (int j = 0; j < layer.height; ++j)
            {
                int index = i * layer.width + j;
                int tileID = layer.data[index];

                GameObject tile = LoadTile(tileID);
                tile.transform.position = new Vector3(j * 0.64f, -i * 0.64f, 0); // the size should be dynamic - TileSize/100*i
                tile.transform.SetParent(layerGo.transform, true);
                if (tile.GetComponent<Collider2D>() != null)
                {
                    lowLevel[index] = 1;
                }
            }
        }
    }

    private void RenderLayerObjects(LevelDescription.Layer layer, GameObject layerGo, List<Tank> tanks)
    {
        for (int objectIndex = 0; objectIndex < layer.objects.Length; ++objectIndex)
        {
            LevelDescription.LayerObject layerObject = layer.objects[objectIndex];
            if (layerObject.type == "Tank")
            {
                Tank tank = Tank.GetConnectedTank();
                if (tank == null && usingTestTank)
                {
                    tank = Tank.GetTank();
                    Debug.Log("Tank created");
                }

                if (tank != null)
                {
                    tank.transform.position = new Vector3(layerObject.x / 100.0f, -layerObject.y / 100.0f, 0);
                    tanks.Add(tank);
                }

                continue;
            }

            if (layerObject.gid >= 0)
            {
                GameObject objectGO = LoadTile(layerObject.gid);
                objectGO.transform.position = new Vector3(layerObject.x / 100.0f, -layerObject.y / 100.0f, 0);
                objectGO.transform.SetParent(layerGo.transform, false);
            }
        }
    }

    public List<Tank> LoadFromDescription(LevelDescription description, bool useOnlyConnectedTanks = true)
    {
        lowLevel = new List<int>();
        Debug.Log("Map width: " + description.width);
        columns = description.width;
        for (int i = 0; i < description.width * description.height; ++i)
        {
            lowLevel.Add(0);
        }
        
        for (int setIndex = 0; setIndex < description.tilesets.Length; ++setIndex)
        {
            LevelDescription.TileSet tileSet = description.tilesets[setIndex];
            LevelDescription.TileSetProperty [] properties = tileSet.tileproperties;
            PrepareTileset(tileSet, properties);
        }

        List<Tank> tanks = new List<Tank>();
        for (int layerIndex = 0; layerIndex < description.layers.Length; layerIndex++)
        {
            LevelDescription.Layer layer = description.layers[layerIndex];
            GameObject layerGo = new GameObject("Layer");
            layerGo.transform.position = new Vector3(0, 0, -layerIndex);
            RenderLayerTiles(layer, layerGo);

            if (layer.objects == null) continue;
            RenderLayerObjects(layer, layerGo, tanks);
        }

        foreach (KeyValuePair<int, GameObject> entry in tiles)
        {
            GameObject.Destroy(entry.Value);
        }

        return tanks;
    }

    private GameObject LoadTile(int tileId)
    {
        try
        {
            GameObject tilePrefab = tiles[tileId];
            GameObject tile = GameObject.Instantiate(tilePrefab) as GameObject;
            return tile;
        }
        catch (System.Exception e)
        {
            Debug.Log("Could not get sprite [" + tileId + "] because: " + e.Message);
        }

        return new GameObject("empty");
    }
}
