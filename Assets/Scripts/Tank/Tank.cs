﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tank : MonoBehaviour {


    #region Static
    private static List<string> schemes = new List<string>() { "BlueTank", "GreenTank", "RedTank", "WhiteTank" };
    private static List<TankScheme> avaliableTankSchemes;
    private static int schemeIndex = 0;

    private static TankScheme GetNextScheme()
    {
        TankScheme scheme = null;
        List<ServerController.TankClient> clients = ServerController.Instance.Clients;
        if (avaliableTankSchemes == null)
        {
            avaliableTankSchemes = new List<TankScheme>();
            foreach (ServerController.TankClient c in clients)
            {
                avaliableTankSchemes.Add(c.Scheme);
            }
        }
        if (avaliableTankSchemes == null || avaliableTankSchemes.Count == 0) return null;

        scheme = avaliableTankSchemes[0];
        avaliableTankSchemes.RemoveAt(0);
        return scheme;
    }

    public static Tank GetConnectedTank()
    {
        TankScheme scheme = GetNextScheme();
        if (scheme != null)
        {
            Tank tank = GameObject.Instantiate(Resources.Load<Tank>("Tank"));
            tank.TankScheme = scheme;
            return tank;
        }

        return null;
    }

    public static Tank GetTank()
    {
        Tank tank = GameObject.Instantiate(Resources.Load<Tank>("Tank"));
        tank.TankScheme = Resources.Load<TankScheme>(schemes[schemeIndex++%schemes.Count]);

        return tank;
    }

    public static void CleanAvaliableTanks()
    {
        avaliableTankSchemes = null;
    }

    #endregion


    public TankScheme TankScheme
    {
        get
        {
            return tankScheme;
        }

        set
        {
            tankScheme = value;
            if (tankScheme != null)
            {
                UpdateTankScheme();
            }
        }
    }

    public float Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value;
        }
    }

    public int Kills
    {
        get;set;
    }

    public int Deaths
    {
        get;set;
    }

    public bool CommunicationSuspended
    {
        get
        {
            return communicationSuspended;
        }

        set
        {
            communicationSuspended = value;
        }
    }

    public GameObject deathEffect;

    public TankBarrelControl barrel;
    public TankMovement movement;

    [SerializeField]
    private SpriteRenderer bodySprite;

    [SerializeField]
    private SpriteRenderer barrelSprite;

    [SerializeField]
    private TankScheme tankScheme;

    private Dictionary<string, System.Action<string>> tankRemoteActions = new Dictionary<string, System.Action<string>>();
    private TankActionsList actionsWrap = new TankActionsList();
    private ServerController.TankClient tankClient;
    private bool communicationSuspended = false;
    private float score;


    public Bullet[] ActiveBullets()
    {
        return barrel.GetActiveBullets();
    }

    private void Awake()
    {
        TankActions[] actions = new TankActions[10];
        for (int i = 0; i < actions.Length; ++i)
        {
            actions[i] = new TankActions();
        }
        actionsWrap.lastActions = actions;

        movement.StartedMovement = TankStartedMovementDelegate;
        movement.StoppedMovement = TankStoppedMovementDelegate;

        barrel.AimCompleted = AimCompleted;
        barrel.CoolDownCompleted = CoolDownCompleted;
        barrel.FireCompleted = Fired;
        barrel.FireHit = FireHit;

        tankRemoteActions.Add("move", MoveRemoteAction);
        tankRemoteActions.Add("movedeg", MoveDegreeRemoteAction);
        tankRemoteActions.Add("stop", StopRemoteAction);

        tankRemoteActions.Add("aim", AimRemoteAction);
        tankRemoteActions.Add("aimdeg", AimDegreeRemoteAction);
        tankRemoteActions.Add("fire", FireRemoteAction);

        tankRemoteActions.Add("info", GetInfo);
    }

    void Start()
    {
        List<ServerController.TankClient> clients = ServerController.Instance.Clients;
        bool connected = false;
        foreach (ServerController.TankClient tankClient in clients)
        {
            if (tankClient.Scheme == this.tankScheme)
            {
                this.tankClient = tankClient;
                tankClient.MessageReceived += TankMessageReceived;
                connected = true;
                break;
            }
        }

        if (!connected)
        {
            ServerController.Instance.TankClientConnected += TankClientConnected;
        }
    }

    private void OnDestroy()
    {
        if (this.tankClient != null)
        {
            tankClient.MessageReceived -= TankMessageReceived;
            tankClient = null;
        }

        if (isQuitting)
        {
            return;
        }

        ServerController.Instance.TankClientConnected -= TankClientConnected;
    }

    private bool isQuitting = false;
    private void OnApplicationQuit()
    {
        isQuitting = true;
    }

    #region Barrel Delegates

    private void AimCompleted(TankBarrelControl barrel)
    {
        string action = "{\"AimCompleted\":{\"angle\":" + barrel.BarrelAngleDegree + "} }";
        SendActionResponseToClient(action);
    }

    private void CoolDownCompleted(TankBarrelControl barrel)
    {
        string action = "{\"CoolDownCompleted\":{\"time\":" + barrel.CoolDownTimer + "} }";
        SendActionResponseToClient(action);
    }

    private void Fired(TankBarrelControl barrel, float angle)
    {
        string action = "{\"FiredAt\":{\"angle\":" + angle + "} }";
        SendActionResponseToClient(action);
    }

    private void FireHit(TankBarrelControl barrel, Collision2D collision)
    {
        string action = "{\"BulletHit\":{\"object\":\"" + collision.transform.tag + "\", \"named\":\"" + collision.transform.name + "\"";
        action += ", \"normal\": " + JsonUtility.ToJson(collision.contacts[0].normal) + ", \"point\": " + JsonUtility.ToJson(collision.contacts[0].point);
        action += "} }";
        if (collision.transform.tag == "Tank")
        {
            Kills++;
            Score += GameController.Instance.gameInfo.ScoreByKill;
        }
        SendActionResponseToClient(action);
    }

    #endregion

    #region Tank Movement Delegates

    private void TankStoppedMovementDelegate(TankMovement mov, Vector2 pos, GameObject go)
    {
        string action = "{\"MovementStopped\":{\"position\":" + JsonUtility.ToJson(pos);
        if (go != null)
        {
            action += ", \"object\":\"" + go.tag + "\"";
        }
        action += "} }";
        SendActionResponseToClient(action);
    }

    private void TankStartedMovementDelegate(TankMovement mov, Vector2 pos, Vector2 vel)
    {
        string action = "{\"MovementStarted\":{\"position\":" + JsonUtility.ToJson(pos) + ",\"velocity\":" + JsonUtility.ToJson(vel) + "} }";
        SendActionResponseToClient(action);
    }

    #endregion

    #region remote actions

    private string TankInfo()
    {
        string myTank = "{";

        myTank += "\"position\":" + JsonUtility.ToJson(transform.position) + ","; // body position
        myTank += "\"rotation\":" + JsonUtility.ToJson(transform.FindChild("takBody").rotation.eulerAngles) + ","; // body rotation
        myTank += "\"lastFireElapsed\":" + barrel.CoolDownTimer + ","; // time, in seconds, last fired
        myTank += "\"canFire\":" + barrel.CanFire.ToString().ToLower() + ","; // if can fire
        myTank += "\"barrelRotation\":" + JsonUtility.ToJson(transform.FindChild("barrel").rotation.eulerAngles) + ","; // barrel rotation, world space, in degrees
        myTank += "\"velocity\":" + JsonUtility.ToJson(transform.GetComponent<Rigidbody2D>().velocity) + ","; //  body velocity
        myTank += "\"history\":" + JsonUtility.ToJson(actionsWrap).Replace("\\", "").Replace("\"{", "{").Replace("}\"", "}") + ","; //  last x actions, from most recent to oldest
        myTank += "\"time\":" + Time.time + ",";
        myTank += "\"round\":" + GameController.Instance.Round;

        myTank += "}";
        return myTank;
    }

    private void GetInfo(string arg)
    {
        tankClient.SendMessage(TankInfo());
    }

    private void MoveRemoteAction(string value)
    {
        float result = 0;
        if (float.TryParse(value, out result))
        {
            movement.Move(result);
            return;
        }

        string errorReport = "{\"error\":{\"description\":\"Could not parse value RAD on tank movement request\", \"value\":" + value + "} }";
        SendActionResponseToClient(errorReport);
    }

    private void MoveDegreeRemoteAction(string value)
    {
        float result = 0;
        if (float.TryParse(value, out result))
        {
            result = Mathf.Deg2Rad * result;
            movement.Move(result);
            return;
        }

        string errorReport = "{\"error\":{\"description\":\"Could not parse value DEG on tank movement request\", \"value\":" + value + "} }";
        SendActionResponseToClient(errorReport);
    }

    private void StopRemoteAction(string value)
    {
        movement.Stop();
    }

    private void AimRemoteAction(string value)
    {
        float result = 0;
        if (float.TryParse(value, out result))
        {
            barrel.Aim(result);
            return;
        }

        string errorReport = "{\"error\":{\"description\":\"Could not parse value RAD in tank aim request\", \"value\":" + value + "} }";
        SendActionResponseToClient(errorReport);
    }

    private void AimDegreeRemoteAction(string value)
    {
        float result = 0;
        if (float.TryParse(value, out result))
        {
            barrel.Aim(result * Mathf.Deg2Rad);
            return;
        }

        string errorReport = "{\"error\":{\"description\":\"Could not parse value DEG in tank aim request\", \"value\":" + value + "} }";
        SendActionResponseToClient(errorReport);
    }

    private void FireRemoteAction(string value)
    {
        if (!barrel.Fire())
        {
            string errorReport = "{\"error\":{\"description\":\"Could not FIRE. Cool down in request\", \"value\":" + barrel.CoolDownTimer + "} }";
            SendActionResponseToClient(errorReport);
        }
    }

    #endregion

    #region Connection And Message Exchange

    private void TankClientConnected(ServerController.TankClient obj)
    {
        if (obj.Scheme == this.tankScheme)
        {
            ServerController.Instance.TankClientConnected -= TankClientConnected;
            this.tankClient = obj;
            obj.MessageReceived += TankMessageReceived;
        }
    }

    private void TankMessageReceived(ServerController.TankClient arg1, string arg2)
    {
        if (communicationSuspended) return;
        string[] args = arg2.Split(new string[] { "(" }, System.StringSplitOptions.RemoveEmptyEntries);
        string command = args[0].Trim().ToLower().Replace(")", "");
        System.Action<string> commandAction = null;
        if (tankRemoteActions.TryGetValue(command, out commandAction))
        {
            string argument = args.Length > 1 ? args[1].Trim().Replace(")", "") : "";
            commandAction(argument);
        }
    }

    private void SendActionResponseToClient(string message)
    {
        actionsWrap.AddAction(message);
        tankClient.SendMessage(message);
    }

    #endregion

    #region Collision

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Bullet")
        {
            gameObject.SetActive(false);
            communicationSuspended = true;
            Deaths++;
            ServerController.Instance.BroadCastMessage("Tank \"" + this.tankScheme.tankName + "\" exploded in a million pieces");
            tankClient.SendMessage("Dead");
            GameObject.Instantiate(deathEffect, transform.position, Quaternion.identity);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
    }

    #endregion

    #region TankScheme

    private void UpdateTankScheme()
    {
        bodySprite.sprite = tankScheme.bodySprite;
        barrelSprite.sprite = tankScheme.barrelSprite;
        barrel.bulletSprite = tankScheme.bulletSprite;
    }

    public void SelectColor(int colorIndex)
    {
        bodySprite.sprite = tankScheme.bodySprite;
        barrelSprite.sprite = tankScheme.barrelSprite;
        barrel.bulletSprite = tankScheme.bulletSprite;
    }

    public void Outline()
    {
        bodySprite.sprite = tankScheme.bodySpriteOutline;
        barrelSprite.sprite = tankScheme.barrelSpriteOutline;
        barrel.bulletSprite = tankScheme.bulletSpriteOutline;
    }

    #endregion

    #region Tank Action History Class Support 

    [System.Serializable]
    private struct TankActionsList
    {
        public TankActions[] lastActions;

        public void AddAction(string action)
        {
            for (int i = lastActions.Length - 1; i >=1 ; --i)
            {
                lastActions[i] = lastActions[i-1];
            }

            lastActions[0] = new TankActions(action, Time.time);
        }
    }

    [System.Serializable]
    private struct TankActions
    {
        public TankActions(string action, float time)
        {
            this.action = action;
            this.time = time;
        }

        public string action;
        public float time;
    }

    #endregion
}
