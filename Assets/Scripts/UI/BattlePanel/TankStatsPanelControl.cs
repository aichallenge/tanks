﻿using UnityEngine;
using System.Collections;

public class TankStatsPanelControl : MonoBehaviour {

    public RectTransform statsParent;
    public TankRoundUI tankStats;

    public void ShowTankStats(Tank [] tanks)
    {
        foreach(Tank tank in tanks)
        {
            TankRoundUI ui = GameObject.Instantiate(tankStats);
            ui.transform.SetParent(statsParent.transform, false);
            ui.LinkTank(tank);
        }
    }
}
