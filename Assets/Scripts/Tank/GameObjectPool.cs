﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameObjectPool : MonoBehaviour {

    public GameObject prefab;

    private List<GameObject> gameObjectPool = new List<GameObject>();

    public GameObject GetObject()
    {
        if (gameObjectPool.Count == 0)
        {
            return GameObject.Instantiate(prefab) as GameObject;
        }

        GameObject go = gameObjectPool[0];
        go.SetActive(true);
        go.SendMessage("Reset", SendMessageOptions.DontRequireReceiver);
        gameObjectPool.RemoveAt(0);
        
        return go;
    }

    public void PutOnPull(GameObject go)
    {
        go.SetActive(false);
        gameObjectPool.Add(go);
    }
}
