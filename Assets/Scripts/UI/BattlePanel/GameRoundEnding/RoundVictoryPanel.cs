﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RoundVictoryPanel : TankUI {

    public Image panel;
    public Image medal;
    public Text timesUpText;
    public Text title;
    public Text subtitle;

    public override void LinkTank(Tank tank)
    {
        if (tank == null)
        {
            title.gameObject.SetActive(false);
            subtitle.gameObject.SetActive(false);
            timesUpText.gameObject.SetActive(true);
            medal.gameObject.SetActive(false);
            tankName.gameObject.SetActive(false);
            tankBody.gameObject.SetActive(false);
            tankBarrel.gameObject.SetActive(false);
            points.gameObject.SetActive(false);
            panel.GetComponent<RectTransform>().sizeDelta = new Vector2(panel.GetComponent<RectTransform>().sizeDelta.x, 250);

            return;
        }

        medal.gameObject.SetActive(GameController.Instance.scoreControl.BestScoreOfAll().tankName == tank.TankScheme.tankName);
        base.LinkTank(tank);
        panel.sprite = tank.TankScheme.UIPanel;
    }

    public void ShowVictoryOf(Tank tank, float secondsOn)
    {
        LinkTank(tank);
        StartCoroutine(ShowFor(secondsOn));
    }

    IEnumerator ShowFor(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        gameObject.SetActive(false);
    }
}
