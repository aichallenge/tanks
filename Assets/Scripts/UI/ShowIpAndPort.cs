﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowIpAndPort : MonoBehaviour {

    public Text text;

	void Start () {
        text.text = ServerController.Instance.Ip + ":" + ServerController.Instance.Port;
	}
}
