﻿using UnityEngine;
using System.Collections;

public class DestroymeAfter : MonoBehaviour {

    public float delay = 2;
	
	IEnumerator Start () {
        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
	}
	
}
