﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class RoundEndedUIControl : MonoBehaviour {

    public RoundEndedPanel roundEndedPanel;
    public RoundVictoryPanel victoryPanel;
    public GameOverStats gameOverPanel;
    public int RoundEndedPanelStaysForSeconds = 5;

    private bool gameOver = false;
	
	void Start () {
        GameController.Instance.RoundEnded += RoundEnded;
        GameController.Instance.GameOver += GameOver;
	}

    private void GameOver(GameController obj)
    {
        gameOver = true;
    }

    private void OnDestroy()
    {
        GameController.Instance.RoundEnded -= RoundEnded;
        GameController.Instance.GameOver -= GameOver;
    }

    Tank GetTankFromScheme(TankScheme scheme)
    {
        if (scheme == null) return null;

        Tank[] tanks = GameController.Instance.Tanks;
        foreach (Tank t in tanks)
        {
            if (t.TankScheme == scheme) return t;
        }

        return null;
    }

    private void RoundEnded(GameController arg1, GameController.RoundEndedMotive arg2)
    {
        StartCoroutine(ClosingRound(arg1, arg2));
    }

    private IEnumerator ClosingRound(GameController arg1, GameController.RoundEndedMotive arg2)
    {
        float timeShowingPanels = 5;
        victoryPanel.gameObject.SetActive(true);
        victoryPanel.ShowVictoryOf(GetTankFromScheme(arg1.LastVictory), timeShowingPanels);
        GetComponent<Image>().color = new Color(0, 0, 0, 0.5f);
        GetComponent<Image>().raycastTarget = true;
        yield return new WaitForSeconds(timeShowingPanels + 0.5f);
        roundEndedPanel.gameObject.SetActive(true);
        roundEndedPanel.Show(RoundEndedPanelStaysForSeconds);
        yield return new WaitForSeconds(timeShowingPanels + 0.5f);
        if (gameOver)
        {
            gameOverPanel.gameObject.SetActive(true);
            gameOverPanel.Show(GameController.Instance.Tanks); // should be in order
        } else
        {
            SceneManager.LoadScene("TankBattle");
        }
    }
}
