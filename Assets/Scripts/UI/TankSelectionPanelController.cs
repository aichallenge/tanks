﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TankSelectionPanelController : MonoBehaviour {

    public Text connectionInfo;
    public Text tankName;
    public Text tankCommands;
    public Toggle readyButton;
    public Image panel;

    public TankScheme initialScheme;
    public Tank tank;

    private ServerController.TankClient tankClient;
    private bool watingConnection = true;


    void Start () {
        Image checkMark = (Image)readyButton.graphic;
        if (checkMark != null)
        {
            checkMark.sprite = initialScheme.UICheckMark;
        }

        tankName.text = initialScheme.tankName;
        panel.sprite = initialScheme.UIPanel;
        tank.TankScheme = initialScheme;
        StartCoroutine(AnimateWaitingConnection());

        ServerController.Instance.TankClientConnected += TankClientConnected;
    }

    private IEnumerator AnimateWaitingConnection()
    {
        while (watingConnection)
        {
            connectionInfo.text = "Waiting Connection";
            yield return new WaitForSeconds(0.25f);
            connectionInfo.text = ".Waiting Connection.";
            yield return new WaitForSeconds(0.25f);
            connectionInfo.text = "..Waiting Connection..";
            yield return new WaitForSeconds(0.25f);
        }

        connectionInfo.text = "Connected";
    }

    private void OnDestroy()
    {
        if (tankClient != null)
            tankClient.MessageReceived -= TankReceivedMessage;
        tankClient = null;
        initialScheme = null;
    }

    private void TankClientConnected(ServerController.TankClient obj)
    {
        if (obj.Scheme == initialScheme)
        {
            watingConnection = false;
            tankClient = obj;
            obj.MessageReceived += TankReceivedMessage;
        }
    }

    private void TankReceivedMessage(ServerController.TankClient arg1, string arg2)
    {
        this.tankCommands.text = arg2;
    }

    private void Update()
    {
        if (tankClient != null)
        {
            tankName.text = tankClient.Name;
            if (tankClient.IsReady)
            {
                readyButton.isOn = true;
                connectionInfo.text = "Ready";
            }
        }
    }
}
